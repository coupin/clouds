import math

def numInt(lower, upper):
    N = [1, 10, 100, 1000, 10000, 100000, 1000000]
    interval = upper - lower
    R=[]
    for i in range(7):
        r = 0
        n = N[i]
        step = interval/n
        for k in range(n):
            r = r + abs(math.sin(lower + k*step))
            #print(r)
        R.append(r*step)
    return R
